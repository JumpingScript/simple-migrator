#!/bin/bash

if [[ $1 == "--setup" ]]
then
    echo "Setting up..."
    if [[ $2 == "--verbose" ]]
    then
        mysql -u root -v < ./setup.sql
    else mysql -u root < ./setup.sql
    fi
elif [[ $1 == "--migrate" ]]
then
    file=$(find updates/ -type f -name "*.sql" | head -n 1)
    echo "Executing $file"
    if [[ $2 == "--verbose" ]]
    then
        mysql -u root -v < "$file"
    else mysql -u root < "$file"
    fi
elif [[ $1 == "--migrate-all" ]]
then
    echo "Migrating all... Starting."
    cat ./updates/*.sql | mysql -u root -v 
    echo "Migrated."
elif [[ $1 == "--migrate-some" ]]
then
    file=$(find updates/ -type f -name "*.sql" | head -n "$2" | tr '\n' ' ')
    echo "Executing $file"
    cat $file | mysql -u root
elif [[ $1 == "--fixtures" ]]
then
    echo "Applying Fixtures"
    if [[ $2 == "--verbose" ]]
    then
        mysql -u root -v < ./fixtures.sql
    else mysql -u root < ./fixtures.sql
    fi
else
    echo -e "Supply parameter '--setup', '--migrate[-all]', '--fixtures'.\n\
Put '--verbose' as the second parameter to see the sql commands.\n \
    --setup             Creates Users and Database, grants permissions. \n \
    --migrate           Applies the latest update, ordered by modification date. \n \
    --migrate-all       Applies all updates ordered by modification date. Does not support verbose output. \
    --migrate-some n    Applies the last n updates ordered by modification date. Does not support verbose output. \
    --fixtures          Applies fixtures."
fi
# Simple-Migrator

This is a simple shell script written to work with (Git) Bash.

Migrator.sh is the main entrypoint, just execute it from the command line.
There's --help.

It works by looking at ./updates/* and executing the sql files under that directory.

Supported Parameters:
<pre>
Put '--verbose' as the second parameter to see the sql commands.
--setup             Should be used to create Users and Database and grants permissions. Looks for ./setup.sql
--migrate           Applies the latest update, ordered by modification date.
--migrate-all       Applies all updates ordered by modification date. Does not support verbose output.
--migrate-some n    Applies the last n updates ordered by modification date. Does not support verbose output.
--fixtures          Applies fixtures. Looks for ./fixtures.sql
</pre>
